import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { FormControl, FormGroup } from '@angular/forms';
import Debug from 'debug';

const debug = Debug('archi:login.component');
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  errorMessage: string = "";
  successMessage: string = "";
  /* email = new FormControl("");
  password = new FormControl(""); */

  LoginForm = new FormGroup({ 
    email: new FormControl(),
    password: new FormControl()
  })

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

  tryLogin(value: any){
    this.authService.login(value);
  }

}
