import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import Debug from 'debug';

const debug = Debug('archi:auth.service');

type LoginFormInputs = {
  email: string,
  password: string
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //isLoggedIn = false;

  // store the URL so we can redirect after logging in
  redirectUrl: string = '/archives';

  constructor(public afAuth: AngularFireAuth){}

  

  login(credentials: LoginFormInputs): Observable<firebase.auth.UserCredential> {
    return from(this.afAuth.signInWithEmailAndPassword(credentials.email, credentials.password)).pipe(
      tap(val => {
        debug(val);
        //this.isLoggedIn = true;
        //return val;
      })
    );
    /* return of(true).pipe(
      delay(1000),
      tap(val => this.isLoggedIn = true)
    ); */
  }

  get isLoggedIn(){
    const isloggedin = Boolean(this.afAuth.user);
    debug('isLoggedIn', isloggedin);
    return isloggedin;
  }

  get user(){
    return this.afAuth.user;
  }

  logout(): Observable<void> {
    //this.isLoggedIn = false;
    return from(this.afAuth.signOut());
  }
}
