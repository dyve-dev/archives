import { Component, OnInit, ElementRef } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../auth/auth.service';


@Component({
  selector: 'app-archives',
  templateUrl: './archives.component.html',
  styleUrls: ['./archives.component.scss']
})
export class ArchivesComponent implements OnInit {
  title = 'Archives';
  userEmail = "";
  
  isDarkTheme = false;
  constructor(private _overlay: OverlayContainer, private _elementRef: ElementRef, private _auth: AuthService) {
  }
  ngOnInit(): void {
    this._auth.user.subscribe(U => {
      this.userEmail = U?.email || "";
    })
  }

  aForm = new FormGroup({
    url: new FormControl("")
  })
 /*  setValue(){
    this.url = "test";
  } */
  clearUrl(){
    this.aForm.setValue({
      url: ""
    })
  }
}
