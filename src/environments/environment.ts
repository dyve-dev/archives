// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyACn44KK647VSK0z-_a5PmssVsRYyGz0fw",
    authDomain: "web-archives-ec7c8.firebaseapp.com",
    projectId: "web-archives-ec7c8",
    storageBucket: "web-archives-ec7c8.appspot.com",
    messagingSenderId: "744743877664",
    appId: "1:744743877664:web:2ff34d188645ba3f1b1045"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
